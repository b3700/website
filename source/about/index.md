---
title: about
logoIcon: fa-user-secret
type: basic
date: 2022-01-24 03:55:51
---
# What
Bnker is hybrid-coworking space for people that needs to be working in motivated environment.
Anyone can visit or work here.
# Who
Currently, bnker is occupied with 5 young individuals.
# Why
We believe that the best environment for work lies between workplace and home.
Sharing problems, insights, knowledge, skills or just talking between people from various domains is in general more effective in terms of finding the right solution for specific problems.
# How
Come visit us and we will answer all of your questions just knock on the door before you enter.
At present, keys to the bnker are shared with each member based on availability and trust but we are working on membership card access.
# Where
Second floor of "Domov sluzieb"
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1300.1177058059504!2d19.547220669791287!3d49.32876172635693!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc0dab70be4f29f9d!2zNDnCsDE5JzQzLjUiTiAxOcKwMzInNTguMSJF!5e0!3m2!1ssk!2ssk!4v1663157717895!5m2!1ssk!2ssk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

# When
In general everyday from 6 am to 17 pm but nothing is static. 
Everything depends on individual needs.