---
title: Bnker
date: 2022-01-22 20:26:55
tags:
---
# Announcement
Bnker is officially open.

# Location
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1300.1177058059504!2d19.547220669791287!3d49.32876172635693!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc0dab70be4f29f9d!2zNDnCsDE5JzQzLjUiTiAxOcKwMzInNTguMSJF!5e0!3m2!1ssk!2ssk!4v1663157717895!5m2!1ssk!2ssk" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

# Available Resources
- Internet 1Gbps
- Kitchen
- Polycarbonate drawing wall
- IT accessories (Monitors, Keyboards, Mouses, Printers etc...)